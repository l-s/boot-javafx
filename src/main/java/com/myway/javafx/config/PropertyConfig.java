package com.myway.javafx.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "myway.system")
public class PropertyConfig {

    private String giturl;
    private String gittoken;

    public String getGiturl() {
        return giturl;
    }

    public void setGiturl(String giturl) {
        this.giturl = giturl;
    }

    public String getGittoken() {
        return gittoken;
    }

    public void setGittoken(String gittoken) {
        this.gittoken = gittoken;
    }
}
