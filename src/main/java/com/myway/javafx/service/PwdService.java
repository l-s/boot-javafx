package com.myway.javafx.service;

import com.myway.javafx.util.DESCrypts;
import org.springframework.stereotype.Component;

@Component
public class PwdService {
    public String encryptPwd(String s) {
        return DESCrypts.EncryptStr(s);
    }
    public String decryptPwd(String str) {
        String truthpwd = "";
        int d1, d2;
        str = str == null ? "" : str;
        while (str.length() > 0) {
            d1 = Integer.parseInt(str.substring(0, 2)) - 32;
            d2 = Integer.parseInt(str.substring(2, 4)) - 32;
            truthpwd += String.valueOf((char) (d1 * 16 + d2));
            str = str.substring(4);
        }
        if (truthpwd.indexOf(",") != -1)
            truthpwd = truthpwd.substring(truthpwd.indexOf(",") + 1);
        return truthpwd;
    }
}
