package com.myway.javafx.entity;

/**
 * @author 梁爽
 * @description GitPro
 * @since 2022-01-27 14:52
 */
public class GitPro {
    public Integer id;
    public String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
